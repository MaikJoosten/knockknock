<?php
header('Content-type: text/html; charset=utf-8');
session_start();

// Prevent unlawful POSTs
if(!isset($_POST['mainSecret']) || $_POST['mainSecret'] !== $_SESSION['mainSecret'] || !isset($_SESSION['id']) || !is_numeric($_SESSION['id'])) {
    exit('What\'s the magic word?');
}


if(isset($_POST['knocksArrPer'])) {
    $_SESSION['correct'] = $_POST['knocksArrPer'];
    echo '{"code": "1", "message": "SAVED!"}';

} else {
    echo 'NO PEEKING!';
}