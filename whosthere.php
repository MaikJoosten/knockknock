<?php
header('Content-type: text/html; charset=utf-8');
session_start();

// Prevent unlawful POSTs
if(!isset($_POST['mainSecret']) || $_POST['mainSecret'] !== $_SESSION['mainSecret'] || !isset($_SESSION['id']) || !is_numeric($_SESSION['id'])) {
    exit('What\'s the magic word?');
}

$tolerancePerKnock = 3;
$allOK = true;

if(isset($_POST['knocksArr'])) {
    $knocksArr = $_POST['knocksArr'];
    $knocksArrPer = array();

    if(!isset($_SESSION['correct'])) {
        $_SESSION['correct'] = array(0, 12.61, 39.65, 61.78, 73.64, 100);
    }
    $correctKnocks = $_SESSION['correct'];

    // Make sure both arrays have the same length
    if(count($knocksArr) === count($correctKnocks)) {

        // Convert knocks array into percetages
        $max = end(array_values($knocksArr));
        for($i = 0; $i < count($knocksArr); $i++) {
            $knocksArrPer[$i] = (int)$knocksArr[$i] / $max * 100;
        }

        // Check if offset is within limits
        for($i = 0; $i < count($knocksArrPer); $i++) {
            if(abs($correctKnocks[$i] - $knocksArrPer[$i]) > $tolerancePerKnock) {
                $allOK = false;
                break;
            }
        }

    } else {
        $allOK = false;
    }

    if($allOK) {
        echo '{"code": "12", "message": "<a href=\"download.php?id=' . $_SESSION['id'] . '&ms=' . $_POST['mainSecret'] . '\" target=\"_blank\">DOWNLOAD LINK</a>"}';

    } else {
        echo '{"code": "0", "message": "WHO\'S THERE?"}';
    }
} else {
    echo 'NO PEEKING!';
}