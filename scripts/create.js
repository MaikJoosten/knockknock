$(document).ready(function() {
    var knocksArr = [],
        knocksArrPer = [],
        timeStart = 0,
        cleanUpTimeout = 0,
        recording = false,
        clickArea = $('#clicker'),
        recordBtn = $('#record'),
        saveBtn = $('#save'),
        showBtn = $('#show'),
        downloadBtn = $('#download'),
        knocks = $('#knocks');

    // Toggle recording
    recordBtn.on('click', function() {
        var t = $(this),
            i,
            k,
            max = knocksArr[knocksArr.length - 1];

        // Visualise knocks
        if(recording) {
            knocks.empty();

            // Convert knock into percentages
            for(i = 0; i < knocksArr.length; i++) {
                knocksArrPer[i] = knocksArr[i] / max * 100;
            }

            // Add knocks visualisation
            for(i = 0; i < knocksArrPer.length; i++) {
                k = $('<div class="knock"></div>');
                k.css({left: knocksArrPer[i] + '%'});
                knocks.append(k);
            }
        } else {
            knocksArr = [];
            knocksArrPer = [];
        }

        t.toggleClass('active');
        recording = !recording;
    });

    // Save recorded knock
    saveBtn.on('click', function() {
        var t = $(this);

        t.addClass('active');
        setTimeout(function() {
            t.removeClass('active');
        }, 100);

        console.log(knocksArrPer);
        $.post('saveme.php', {knocksArrPer: knocksArrPer, mainSecret: mainSecret}, function(data) {

            // Make sure the returned data is JSON
            try {
                data = $.parseJSON(data);

                // Saved
                if(data.code > 0) {

                // Error
                } else {

                }

            } catch(e) {
                console.log('JSON error');
            }
        });
    });

    // Show recorded knock
    showBtn.on('click', function() {

        // Show recorded knock
        for(var i = 0; i < knocksArr.length; i++) {
            var dot;

            setTimeout(function() {
                dot = $('<div class="dot"></div>');
                dot.css({
                    left: (clickArea.width() / 2) + 'px',
                    top: (clickArea.outerHeight() / 2) + 'px'
                });
                clickArea.append(dot);
            }, knocksArr[i]);
        }

        // Remove dots
        setTimeout(function() {
            clickArea.empty();
        }, knocksArr[knocksArr.length - 1] + 2000);
    });

    // Download recorded knock
    downloadBtn.on('click', function() {
        var data = [],
            lengthInSec = knocksArr[knocksArr.length - 1] / 1000,
            contentType = 'audio/wav',
            uInt8Array,
            riffwave = new RIFFWAVE(),
            rawLength,
            audioBlob,
            sampleRateHz = 44100,
            knockArrPerRounded = [],
            knockArrPerKeys = [],
            notes = [];

        for(i = 0; i < knocksArrPer.length; i++) {
            knockArrPerRounded[i] = Math.round(knocksArrPer[i]);
        }
        knockArrPerKeys = array_flip(knockArrPerRounded);

        for(var i = 0; i < 110; i++) {
            notes[i] = (knockArrPerKeys[i] !== undefined) ? 75 : 0;
        }

        function baseFreq(index) {
            return 2 * Math.PI * 440.0 * Math.pow(2, (notes[index] - 69) / 12.0) / sampleRateHz;
        }

        for(var j = 0; j < lengthInSec * sampleRateHz; j++) {
            var l = lengthInSec * sampleRateHz / notes.length;
            data[j] = 64 + 32 * Math.round(Math.sin(baseFreq(Math.round(j / l)) * j));
        }

        riffwave.header.sampleRate = sampleRateHz;
        riffwave.header.numChannels = 1;
        riffwave.Make(data);
        rawLength = riffwave.wav.length;
        uInt8Array = new Uint8Array(rawLength);

        for(var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = riffwave.wav[i];
        }
        audioBlob = new Blob([uInt8Array], {type: contentType});
        forceDownload(audioBlob);
    });

    /* KnockKnock */
    clickArea.on('click', function(e) {
        var t = $(this),
            dot;

        // Cleanup previous classes
        t.removeClass('active');
        clearTimeout(cleanUpTimeout);

        // Visualise knock
        dot = $('<div class="dot"></div>');
        dot.css({
            left: e.pageX + 'px',
            top: e.pageY + 'px'
        });
        t.append(dot);

        // First in series
        if(knocksArr.length == 0) {
            timeStart = Date.now();
            knocksArr.push(0);

        // Continuation
        } else {
            knocksArr.push(Date.now() - timeStart);
        }

        // Cleanup dots
        cleanUpTimeout = setTimeout(function() {
            clickArea.empty();
        }, 2000);
    });
});

/* Force download of knock audio */
function forceDownload(blob) {
    var url = (window.URL || window.webkitURL).createObjectURL(blob),
        link = window.document.createElement('a'),
        click = document.createEvent('Event');

    link.href = url;
    link.download = 'output.wav';
    click.initEvent('click', true, true);
    link.dispatchEvent(click);
}

function array_flip(arr) {
    var key,
        tmp_arr = {};

    for(key in arr) {
        if(arr.hasOwnProperty(key)) {
            tmp_arr[arr[key]] = key;
        }
    }

    return tmp_arr;
}