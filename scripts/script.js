$(document).ready(function() {
    var knocksArr = [],
        checkKnocksTimeout = 0,
        knockCheckWaitingTime = 1000,
        timeStart = 0,
        clickArea = $('#clicker');

    /* KnockKnock */
    clickArea.on('click tap', function(e) {
        e.preventDefault();

        var t = $(this),
            dot,
            pageX = e.pageX;
            pageY = e.pageY;

        // Use alternative event for touch devices
        if(e.originalEvent.targetTouches && e.originalEvent.targetTouches.length) {
            pageX = e.originalEvent.targetTouches[0].pageX;
            pageY = e.originalEvent.targetTouches[0].pageY;
        }

        // Prevent reset
        clearTimeout(checkKnocksTimeout);

        // Cleanup previous classes
        t.removeClass('incorrect correct active');

        // Visualise knock
        dot = $('<div class="dot"></div>');
        dot.css({
            left: pageX + 'px',
            top: pageY + 'px'
        });
        t.append(dot);

        // First in series
        if(knocksArr.length == 0) {
            timeStart = Date.now();
            knocksArr.push(0);

        // Continuation
        } else {
            knocksArr.push(Date.now() - timeStart);
        }

        // Long time no knock; check result
        checkKnocksTimeout = setTimeout(function() {

            // Cleanup dots
            $('.dot', clickArea).remove();

            // Send knock to PHP
            $.post('whosthere.php', {knocksArr: knocksArr, mainSecret: mainSecret}, function(data) {

                // Make sure the returned data is JSON
                try {
                    data = $.parseJSON(data);

                    // Correct knock
                    if(data.code > 0) {
                        clickArea.off();
                        t.addClass('correct').find('.content').html(data.message);

                    // Wrong knock
                    } else {
                        t.addClass('incorrect').find('.content').html(data.message);
                        setTimeout(function() {
                            t.removeClass('incorrect').find('.content').html('');
                        }, 1500);
                    }

                } catch(e) {
                    console.log('JSON error');
                }

                // Reset everything
                knocksArr = [];
                timeStart = 0;
            });
        }, knockCheckWaitingTime);
    });
});