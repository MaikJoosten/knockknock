<?php
header('Content-type: text/html; charset=utf-8');
session_start();

// Prevent unlawful GETs
if(!isset($_GET['ms']) || $_GET['ms'] !== $_SESSION['mainSecret'] || !isset($_SESSION['id']) || !is_numeric($_SESSION['id']) || $_GET['id'] != $_SESSION['id']) {
    exit('What\'s the magic word?');
}

if(isset($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $file = __DIR__ . '/uploads/knocker.jpg';
    if(file_exists($file)) {
        header('Pragma: public');
        header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Disposition: attachment; filename=' . basename($file));
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        if(readfile($file)) {
            exit;
        }
    }
}
echo 'Something went wrong!';