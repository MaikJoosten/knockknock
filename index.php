<?php
session_start();

// Set main secret
$_SESSION['mainSecret'] = md5(time() * 0.21684683);

if(isset($_GET['id']) && is_numeric($_GET['id'])) {
    $_SESSION['id'] = $_GET['id'];

} else {
    $_SESSION['id'] = 156;
}
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <title>KnockKnock</title>
        <link rel="stylesheet" href="styles/style.css">
    </head>
    <body>
        <div id="clicker" class="fullscreen">
            <div class="content"></div>
        </div>
        <div id="create"><a href="create">create</a></div>
    </body>
    <script src="scripts/jquery-1.10.1.min.js"></script>
    <script src="scripts/script.js"></script>
    <script>var mainSecret = '<?php echo $_SESSION['mainSecret']; ?>';</script>
</html>