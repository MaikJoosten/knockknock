<?php
session_start();

// Set main secret
$_SESSION['mainSecret'] = md5(time() * 0.21684683);
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>KnockKnock</title>
        <link rel="stylesheet" href="styles/style.css">
    </head>
    <body>
        <div id="clicker"></div>
        <div id="record">Record</div>
        <div id="save">Save</div>
        <div id="show">Show</div>
        <div id="download">Download audio</div>
        <a href="./" id="test">Test it out!</a>
        <div id="knocks"></div>
    </body>
    <script src="scripts/jquery-1.10.1.min.js"></script>
    <script src="scripts/riffwave.js"></script>
    <script src="scripts/create.js"></script>
    <script>var mainSecret = '<?php echo $_SESSION['mainSecret']; ?>';</script>
</html>